---

## Drag And Drop


简介： 按下 VR 控制器的 Trigger 按键，可以捡起小球并投掷。

调试难度： ★★★★★

基于提供的场景，完成以下tasks：

1. 设计并完成 VR 保龄球    （难度： ★★）
2. 设计并完成 VR 投篮游戏  （难度： ★★★）
3. 设计并完成 VR 3D 拼图   （难度： ★★★★）
4. 制作 会闪避躲藏的 AI 并完成 VR 躲避球游戏 （难度： ★★★★★）

![Drag And Drop](https://bitbucket.org/blueprintrealityinc/vr_demo_instruction/raw/master/demo3.png "快来愉快地开始你的第一个VR App吧")

---

Acknowledge

https://assetstore.unity.com/packages/templates/systems/steamvr-plugin-32647

http://www.leadingones.com/articles/throwing-objects.html